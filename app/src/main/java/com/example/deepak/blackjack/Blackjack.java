package com.example.deepak.blackjack;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.ScrollView;
import android.widget.Button;
import java.util.ArrayList;
import java.util.Random;
import android.os.Vibrator;

/**
 *
 * This class loads BlackJack
 *
 */

public class Blackjack extends AppCompatActivity {

    // Gives the hand Tablelayout
    private TableLayout handsTableLayout;
    // TextView to display scores.
    private TextView winCountTextView;
    private TextView lossCountTextView;
    private TextView tieCountTextView;

    //Hit, Stay and NewButton
    private Button newGameButton;
    private Button hitButton;
    private Button stayButton;

    // variables to store hand Count, cards count, win , loss and tie count
    private int handCount = 0;
    private int playerCardsCount = 3;
    private int dealerCardsCount = 2;
    private int playerHandScore = 0;
    private int dealerHandScore = 0;
    private int winCount = 0;
    private int lossCount = 0;
    private int tieCount = 0;

    // deck to store cards
    private ArrayList<String> deck;
    private static final String cardFormat= "%s %s";
    private static final String LOST = "LOST";
    private static final String TIE = "TIE";
    private static final String WON = "WON";
    private static final String Score = "Player Score %s\nDealer Score %s";
    //Suite values
    private static final String[] SUITES = new String[] {"C","H","S","D"}; // club, heart, spade, diamond
    //FaceValue
    private static final String[] CARDS = new String[] {"A", "2", "3", "4", "5", "6", "7", "8" ,"9" ,"X", "J", "Q", "K"};
    private static final String BLACKJACK_MESSAGE = "%s hits BlackJack :)";


    /**
     * This method renders the view
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blackjack);
        deck = fillDeck();
        setCountTextViews();
        setButtons();
        handsTableLayout = (TableLayout) findViewById(R.id.handsTableLayout);
        winCountTextView = (TextView) findViewById(R.id.winCountTextView);
        lossCountTextView = (TextView) findViewById(R.id.lossCountTextView);
        tieCountTextView = (TextView) findViewById(R.id.tieCountTextView);
        addHand();
    }

    /**
     * This method sets Win, Loss and Tie Count TextViews
     */
    private void setCountTextViews() {
        winCountTextView = (TextView) findViewById(R.id.winCountTextView);
        lossCountTextView = (TextView) findViewById(R.id.lossCountTextView);
        tieCountTextView = (TextView) findViewById(R.id.tieCountTextView);
    }

    /**
     * THis method sets hit, newGame, stay buttons and their listeners
     *
     */
    private void setButtons() {
        newGameButton = (Button) findViewById(R.id.newGameButton);
        newGameButton.setOnClickListener(newGameButtonListener);

        hitButton = (Button) findViewById(R.id.hitButton);
        hitButton.setOnClickListener(hitButtonListener);

        stayButton = (Button) findViewById(R.id.stayButton);
        stayButton.setOnClickListener(stayButtonListener);
    }

    /**
     * this method moves the scroll to bottom
     *
     */
    private void moveScrollToBottom() {
        final ScrollView scrollView = (ScrollView)findViewById(R.id.handScrollView);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }

    /**
     * This method adds new hand to game.
     *
     */
    private void addHand() {

        handCount++;
        resetVariablesOnNewHand();
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View newHand = inflater.inflate(R.layout.hand_layout, null);
        newHand.setId(handCount);
        handsTableLayout.addView(newHand);
        moveScrollToBottom();

        TextView newHandTextView = (TextView)newHand.findViewById(R.id.newHandTextView);
        newHandTextView.setText(newHandTextView.getText() + " " + handCount);

        // dealer card 1
        TextView dealerCard1 = (TextView)newHand.findViewById(R.id.dealerCard1);
        String cardText = getRandomCard();
        dealerCard1.setText(cardText);
        setCardColor(dealerCard1,cardText);
        dealerHandScore = calculateScore(cardText, dealerHandScore);

        // player card 1
        TextView playerCard1 = (TextView)newHand.findViewById(R.id.playerCard1);
        cardText = getRandomCard();
        playerCard1.setText(cardText);
        setCardColor(playerCard1, cardText);
        playerHandScore = calculateScore(cardText, playerHandScore);

        // player card2
        TextView playerCard2 = (TextView)newHand.findViewById(R.id.playerCard2);
        cardText = getRandomCard();
        playerCard2.setText(cardText);
        setCardColor(playerCard2, cardText);
        playerHandScore = calculateScore(cardText, playerHandScore);
        if(playerHandScore == 21) {
            showDialogBox(String.format(BLACKJACK_MESSAGE, "Player"),R.string.blackjack);
            hitButton.setEnabled(false);
            stayButton.setEnabled(false);
            openDealerCard();
        }

    }

    /**
     * This method reset Variables on addition of new hand.
     *
     */
    private void resetVariablesOnNewHand() {
        playerCardsCount = 3;
        dealerCardsCount = 2;
        playerHandScore = 0;
        dealerHandScore = 0;
        hitButton.setEnabled(true);
        stayButton.setEnabled(true);
    }

    /**
     * This method set colors to cards according to theor value
     *
     * @param card
     * @param cardText
     */
    private void setCardColor(TextView card, String cardText) {
        if(cardText.contains("C") || cardText.contains("S")){
            card.setTextColor(getResources().getColor(R.color.black));
        } else {
            card.setTextColor(getResources().getColor(R.color.red));
        }

    }

    /**
     * This method picks randam card out of deck. and then remove the card from deck
     *
     * If count of cards is <=10, then it creates new deck.
     *
     * @return
     */
    private String getRandomCard() {
        Random random = new Random();
        if (deck.size() <= 10) {
            deck = fillDeck();
        }
        int randomCardIndex = random.nextInt(deck.size());
        String card = deck.get(randomCardIndex);
        deck.remove(randomCardIndex);
        return card;
    }

    /**
     * New Game Button Listener
     *
     * // remove all views, reset variables. fills new deck
     *
     */
    public OnClickListener newGameButtonListener = new OnClickListener(){
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(Blackjack.this);
            builder.setTitle(R.string.confirmTitle);
            builder.setPositiveButton(R.string.newGame, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int button) {

                    handsTableLayout.removeAllViews();
                    handCount = 0;
                    resetVariablesOnNewHand();
                    deck = fillDeck();
                    winCount = 0;
                    lossCount = 0;
                    tieCount = 0;
                    winCountTextView.setText(winCount + "");
                    tieCountTextView.setText(tieCount + "");
                    lossCountTextView.setText(lossCount + "");
                    addHand();
                } // ok onCLick
            }); // positive button
            builder.setCancelable(true);
            builder.setNegativeButton(R.string.cancel, null);
            builder.setMessage(R.string.confirmMessage);
            AlertDialog confirmDialog = builder.create();
            confirmDialog.show();
        }
    };

    /**
     *
     * This method opens a card for player
     */
    private void openPlayerCard() {
        View v =  findViewById(handCount);
        TextView playerCard = null;
        if(playerCardsCount == 3) {
            playerCard = (TextView)v.findViewById(R.id.playerCard3);
        } else if(playerCardsCount == 4) {
            playerCard = (TextView)v.findViewById(R.id.playerCard4);
        } else if(playerCardsCount == 5) {
            playerCard = (TextView)v.findViewById(R.id.playerCard5);
        } else if(playerCardsCount == 6) {
            playerCard = (TextView)v.findViewById(R.id.playerCard6);
        } else if(playerCardsCount == 7) {
            playerCard = (TextView)v.findViewById(R.id.playerCard7);
        } else if(playerCardsCount == 8) {
            playerCard = (TextView)v.findViewById(R.id.playerCard8);
        } else {
            playerCard = (TextView)v.findViewById(R.id.playerCard9);
        }

        String cardText = getRandomCard();
        playerCard.setText(cardText);
        setCardColor(playerCard, cardText);

        playerHandScore = calculateScore(cardText, playerHandScore);
        playerCardsCount++;
        if(playerHandScore == 21) {
            showDialogBox(String.format(BLACKJACK_MESSAGE, "Player"),R.string.blackjack);
            hitButton.setEnabled(false);
            stayButton.setEnabled(false);
            openDealerCard();
        } else if(playerHandScore >21) {
            setAndShowResults(LOST);
            TextView handTextView = (TextView)v.findViewById(R.id.newHandTextView);
            handTextView.setTextColor(getResources().getColor(R.color.red));
        }

    }

    /**
     * This method opens a card for player
     *
     */
    private void openDealerCard() {
        View v =  findViewById(handCount);
        TextView dealerCard = null;
        while(dealerHandScore < 17) {
            if (dealerCardsCount == 2) {
                dealerCard = (TextView) v.findViewById(R.id.dealerCard2);
            } else if (dealerCardsCount == 3) {
                dealerCard = (TextView) v.findViewById(R.id.dealerCard3);
            } else if (dealerCardsCount == 4) {
                dealerCard = (TextView) v.findViewById(R.id.dealerCard4);
            } else if (dealerCardsCount == 5) {
                dealerCard = (TextView) v.findViewById(R.id.dealerCard5);
            } else if (dealerCardsCount == 6) {
                dealerCard = (TextView) v.findViewById(R.id.dealerCard6);
            } else if (dealerCardsCount == 7) {
                dealerCard = (TextView) v.findViewById(R.id.dealerCard7);
            } else if (dealerCardsCount == 8) {
                dealerCard = (TextView) v.findViewById(R.id.dealerCard8);
            } else {
                dealerCard = (TextView) v.findViewById(R.id.dealerCard9);
            }
            String cardText = getRandomCard();
            dealerCard.setText(cardText);
            setCardColor(dealerCard, cardText);


            dealerHandScore = calculateScore(cardText, dealerHandScore);
            dealerCardsCount++;
        }
        if(dealerHandScore == 21) {
            showDialogBox(String.format(BLACKJACK_MESSAGE, "Dealer"),R.string.blackjack);
        }
        if(dealerHandScore > 21 || dealerHandScore < playerHandScore) {
            setAndShowResults(WON);
            TextView handTextView = (TextView) v.findViewById(R.id.newHandTextView);
            handTextView.setTextColor(getResources().getColor(R.color.green));
        } else if(dealerHandScore > playerHandScore ) {
            setAndShowResults(LOST);
            TextView handTextView = (TextView)v.findViewById(R.id.newHandTextView);
            handTextView.setTextColor(getResources().getColor(R.color.red));
        } else {
            setAndShowResults(TIE);
            TextView handTextView = (TextView)v.findViewById(R.id.newHandTextView);
            handTextView.setTextColor(getResources().getColor(R.color.blue));
        }
       // addHand();
    }

    /**
     * This method makes the device to vibrate for sometime after lossing a game.
     *
     */
    private void shakeDevice() {
        Vibrator vibrate = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        vibrate.vibrate(500);
    }

    /**
     * This method sets variables of win, loss and tie and opens a dialog box.
     *
     * @param result
     */
    private void setAndShowResults(final String result) {
        String message ="";
        int titleId = 0;
        if(result.equals(LOST)) {
            lossCount++;
            lossCountTextView.setText(lossCount+"");
            message = String.format(Score, playerHandScore,dealerHandScore);
            titleId = R.string.lost;
            shakeDevice();
        }  else if (result.equals(WON)) {
            winCount++;
            winCountTextView.setText(winCount + "");
            message = String.format(Score, playerHandScore, dealerHandScore);
            titleId = R.string.won;
        } else {
            tieCount ++;
            tieCountTextView.setText(tieCount + "");
            message = String.format(Score, playerHandScore, dealerHandScore);
            titleId = R.string.tie;
        }
        showDialogBox(message, titleId);
    }

    /**
     * This method opens a dialog box.
     *
     * @param message
     * @param title
     */
    private void showDialogBox(final String message, final int title) {
        // create a new AlertDialog Builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title); // title bar string
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(title != R.string.blackjack) {
                            addHand();
                        }
                    } // end method onClick
                } // end anonymous inner class
        ); // end call to setPositiveButton

        // create AlertDialog from the Builder
        AlertDialog resetDialog = builder.create();
        resetDialog.show(); // display the Dialog
    }

    /**
     * Hit button Listener
     */
    public OnClickListener hitButtonListener = new OnClickListener(){
        @Override
        public void onClick(View v) {
            openPlayerCard();
        }
    };

    /**
     * This method sets handscore according to the value of card and return total score
     *
     * @param cardText
     * @param handScore
     * @return
     */
    private int calculateScore(String cardText, int handScore) {
        String[] split = cardText.split("\\s+");
        String faceValue = split[0];
        if(faceValue.equals("X") || faceValue.equals("J") || faceValue.equals("Q") || faceValue.equals("K")) {
            faceValue = "10";
        } else if(faceValue.equals("A")) {
            if (handScore <= 10) {
                faceValue = "11";
            } else {
                faceValue = "1";
            }
        }
        handScore += Integer.parseInt(faceValue);
        return handScore;
    }

    /**
     * Stay Button Listener
     */
    public OnClickListener stayButtonListener = new OnClickListener(){
        @Override
        public void onClick(View v) {
            hitButton.setEnabled(false);
            stayButton.setEnabled(false);
            openDealerCard();
        }
    };

    /**
     * This method fills the deck with cards
     * @return
     */
    private ArrayList<String> fillDeck() {
        ArrayList<String> deck =  new ArrayList<String>();

        for(String suite: SUITES) {
            for(String card: CARDS) {
                deck.add(String.format(cardFormat,card,suite));
            }
        }
        return deck;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_blackjack, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
